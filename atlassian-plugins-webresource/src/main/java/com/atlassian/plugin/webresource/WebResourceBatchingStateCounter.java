package com.atlassian.plugin.webresource;

/**
 * @since 2.11.0
 */
public interface WebResourceBatchingStateCounter
{
    /***
     * Gets batching state counter. This counter increments every time there is a state change in any plugin
     * inside the plugin framework such as a plugin being disabled or a new plugin being installed.
     * This counter thus can be used to determine the validity of the current batch resources since a disabling/
     * enabling/upgrade of a plugin can result in relevant resources being updated or inserted into the batch.
     *
     * @return batching state counter
     */
    long getBatchingStateCounter();

    /**
     * Increment of the counter. This is often useful when there is a need to flush the batch resources
     * explicitly as it is known that the current batch resources will be no longer relevant and that the plugins
     * framework will not be notified by any kind of plugin event. An example is when there is a change in
     * system theme which does not involve loading/unloading of plugins.
     */
    void incrementCounter();
}
