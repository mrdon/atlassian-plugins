package com.atlassian.plugin.loaders.classloading;

import com.atlassian.plugin.test.PluginTestUtils;
import com.atlassian.plugin.util.ClassLoaderUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class DirectoryPluginLoaderUtils
{
    private static final String TEST_PLUGIN_DIRECTORY = "ap-plugins";

    /**
     * Copies the test plugins to a new temporary directory and returns that directory.
     */
    public static File copyTestPluginsToTempDirectory() throws IOException
    {
        File directory = PluginTestUtils.createTempDirectory(DirectoryPluginLoaderUtils.class);
        FileUtils.copyDirectory(getTestPluginsDirectory(), directory);

        // Clean up version control files in case we copied them by mistake.
        FileUtils.deleteDirectory(new File(directory, "CVS"));
        FileUtils.deleteDirectory(new File(directory, ".svn"));

        return directory;
    }

    /**
     * Returns the directory on the classpath where the test plugins live.
     */
    public static File getTestPluginsDirectory()
    {
        URL url = ClassLoaderUtils.getResource(TEST_PLUGIN_DIRECTORY, DirectoryPluginLoaderUtils.class);
        return new File(url.getFile());
    }
}
