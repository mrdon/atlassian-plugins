package com.atlassian.plugin.web.model;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.renderer.RendererException;
import com.atlassian.plugin.web.renderer.StaticWebPanelRenderer;
import com.atlassian.plugin.web.renderer.WebPanelRenderer;
import com.google.common.base.Preconditions;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * @since   2.5.0
 */
public abstract class AbstractWebPanel implements WebPanel
{
    private final PluginAccessor pluginAccessor;
    protected Plugin plugin;
    private String resourceType;

    protected AbstractWebPanel(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    public void setPlugin(Plugin plugin)
    {
        this.plugin = plugin;
    }

    public void setResourceType(String resourceType)
    {
        this.resourceType = Preconditions.checkNotNull(resourceType);
    }

    /**
     * Default implementation of {@link WebPanel#writeHtml(java.io.Writer, java.util.Map)} that delegates to
     * {@link WebPanel#getHtml(java.util.Map)}. This method is provided for backwards compatibility with
     * pre-2.11 implementations of WebPanel. Panels SHOULD be upgraded to support this method natively
     * for performance reasons.
     *
     * @param writer the writer to append the panel output to
     * @param context the contextual information that can be used during
     *  rendering. Context elements are not standardized and are
     *  application-specific, so refer to your application's documentation to
     *  learn what is available.
     * @throws IOException
     */
    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException
    {
        writer.write(getHtml(context));
    }

    protected final WebPanelRenderer getRenderer()
    {
        if (StaticWebPanelRenderer.RESOURCE_TYPE.equals(resourceType))
        {
            return StaticWebPanelRenderer.RENDERER;
        }
        else
        {
            for (WebPanelRenderer webPanelRenderer : pluginAccessor.getEnabledModulesByClass(WebPanelRenderer.class))
            {
                if (Preconditions.checkNotNull(resourceType).equals(webPanelRenderer.getResourceType()))
                {
                    return webPanelRenderer;
                }
            }
            throw new RendererException("No renderer found for resource type: " + resourceType);
        }
    }
}
